export interface WPTextField {
  protected?: boolean
  rendered: string
}

export type WPPostId = number
export type WPUserId = number
export type Uri = string
export type DateTime = string
