import { DateTime, WPTextField, WPPostId, Uri, WPUserId } from './common'

export interface WPPost {
  date: DateTime
  date_gmt: DateTime
  guid: WPTextField
  id: WPPostId
  link: Uri
  modified: DateTime
  modified_gmt: DateTime
  slug: string
  status: 'publish' | 'future' | 'draft' | 'pending' | 'private'
  type: string
  password: string
  title: WPTextField
  content: WPTextField
  author: WPUserId
  excerpt: WPTextField
  featured_media: WPPostId
  comment_status: 'open' | 'closed'
  ping_status: 'open' | 'closed'
  format:
    | 'standard'
    | 'aside'
    | 'chat'
    | 'gallery'
    | 'link'
    | 'image'
    | 'quote'
    | 'status'
    | 'video'
    | 'audio'
  meta: unknown
  sticky: boolean
  template: string
  categories: WPPostId[]
  tags: number[]
  [extraKey: string]: unknown
}
